package com.endava.internship;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class HashMapImpl<K, V> implements Map<K, V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final float LOAD_FACTOR = 0.75F;

    private Node<K, V>[] table;
    private int size;

    @SuppressWarnings("unchecked")
    public HashMapImpl() {
        table = new Node[DEFAULT_INITIAL_CAPACITY];
    }

    @SuppressWarnings("unchecked")
    public HashMapImpl(int initialCapacity) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        table = new Node[initialCapacity];
    }

    public HashMapImpl(Map<? extends K, ? extends V> m) {
        this(m.size());
        putAll(m);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return getNodeByKey(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return entrySet().stream()
                .anyMatch(e -> e.getValue().equals(value));
    }

    @Override
    public V get(Object key) {
        Node<K, V> curr = getNodeByKey(key);
        return curr == null ? null : curr.value;
    }

    private Node<K, V> getNodeByKey(Object key) {
        int bucketIndex = getBucketIndex(key);
        Node<K, V> curr = table[bucketIndex];

        if (curr != null) {
            while (curr != null && !Objects.equals(curr.key, key))
                curr = curr.next;
            return curr;
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        int bucketIndex;

        if (table.length == 0) resize();
        bucketIndex = getBucketIndex(key);
        Node<K, V> newNode = new Node<>(key, value);

        if (table[bucketIndex] == null) { // first element in the bucket
            table[bucketIndex] = newNode;
            size++;
            checkLoadFactor();
        } else {
            Node<K, V> curr = table[bucketIndex];
            boolean rewriteEntry = Objects.equals(curr.key, key);

            if (!rewriteEntry) { //first key in the bucket is not equal
                while (!(rewriteEntry = Objects.equals(curr.key, key)) && curr.next != null) {
                    curr = curr.next;
                }
            }
            if (rewriteEntry) { // rewriting old value
                V oldVal = curr.value;
                curr.value = newNode.value;
                return oldVal;
            } else {
                curr.next = newNode;
                size++;
                checkLoadFactor();
            }
        }
        return null;
    }

    private void checkLoadFactor() {
        if (((float) size) / table.length > LOAD_FACTOR) {
            resize();
        }
    }

    @SuppressWarnings("unchecked")
    private void resize() {
        Node<K, V>[] oldTab = table;
        int newLen = table.length == 0 ? DEFAULT_INITIAL_CAPACITY : table.length * 2;
        table = new Node[newLen];
        size = 0;
        for (int i = 0; i < oldTab.length; i++) {
            Node<K, V> curr = oldTab[i];
            if (curr != null) {
                put(curr.key, curr.value);
                while (curr.next != null) {
                    curr = curr.next;
                    put(curr.key, curr.value);
                }
            }
        }
    }

    private int getBucketIndex(Object key) {
        if (key == null) return 0;
        int hash = key.hashCode();
        return Math.abs(hash) % table.length;
    }

    @Override
    public V remove(Object key) {
        int bucketIndex = getBucketIndex(key);
        Node<K, V> prev = null;
        Node<K, V> curr = table[bucketIndex];
        while (curr != null) {
            if (Objects.equals(curr.key, key)) {
                if (prev == null) {
                    table[bucketIndex] = curr.next;
                } else {
                    prev.next = curr.next;
                }
                size--;
                return curr.getValue();
            }
            prev = curr;
            curr = curr.next;
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public void clear() {
        int newLen = table.length;
        table = new Node[newLen];
        size = 0;
    }

    @Override
    public Set<K> keySet() {
        Set<K> set = new HashSet<>();
        for (int i = 0; i < table.length; i++) {
            Node<K, V> curr = table[i];
            if (curr != null) {
                set.add(curr.getKey());
                while (curr.next != null) {
                    curr = curr.next;
                    set.add(curr.getKey());
                }
            }
        }
        return set;
    }

    @Override
    public Collection<V> values() {
        Collection<V> collection = new ArrayList<>();
        for (int i = 0; i < table.length; i++) {
            Node<K, V> curr = table[i];
            if (curr != null) {
                collection.add(curr.getValue());
                while (curr.next != null) {
                    curr = curr.next;
                    collection.add(curr.getValue());
                }
            }
        }
        return collection;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        /* TODO
             doesn't throw UnsupportedOperationException for now,
             so toString can work properly, we should decide
             whether I should implement entrySet to behave as a view,
             or should override toString
         */
        Set<Entry<K, V>> set = new HashSet<>();
        for (int i = 0; i < table.length; i++) {
            Node<K, V> curr = table[i];
            if (curr != null) {
                set.add(new Node<>(curr.key, curr.value));
                while (curr.next != null) {
                    curr = curr.next;
                    set.add(new Node<>(curr.key, curr.value));
                }
            }
        }
        return set;
    }


    private static class Node<K, V> implements Map.Entry<K, V> {
        final K key;
        final int hash;
        V value;

        Node<K, V> next;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            hash = key == null ? 0 : key.hashCode();
            next = null;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V valueBefore = this.value;
            this.value = value;
            return valueBefore;
        }

        @Override
        public String toString() {
            return key + "=" + value;
        }
    }

    @Override
    public String toString() {
        return entrySet().stream()
                .map(Object::toString)
                .collect(Collectors.joining(",", "{", "}"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return this.toString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}
