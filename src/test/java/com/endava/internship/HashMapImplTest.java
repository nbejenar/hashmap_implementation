package com.endava.internship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static com.endava.internship.util.TestUtils.JOHN;
import static com.endava.internship.util.TestUtils.NICK;
import static com.endava.internship.util.TestUtils.OLEG;
import static org.assertj.core.api.Assertions.assertThat;


class HashMapImplTest {

    Map<Student, Integer> map;

    @BeforeEach
    void setUp() {
        map = new HashMapImpl<>();
    }

    @Test
    void whenJustInitialized_isEmptyShouldReturnTrue() {
        assertThat(map).isEmpty();
    }

    @Test
    void whenPutOneEntry_isEmptyShouldReturnFalse() {
        map.put(OLEG, 10);

        assertThat(map).isNotEmpty();
    }

    @Test
    void whenPutOneEntry_SizeShouldBeOne() {
        map.put(NICK, 19);

        assertThat(map).hasSize(1);
    }

    @Test
    void toStringShouldReturnProperString() {
        map.put(OLEG, 11);
        assertThat(map).hasToString("{Student{name='Oleg', dateOfBirth=2001-08-28}=11}");
    }

    @Test
    void shouldReturnStringOfEmptyMap() {
        map.put(NICK, null);
        map.remove(NICK);

        assertThat(map.toString()).hasToString("{}");
    }

    @Test
    void mapsShouldNotBeEqual() {
        Map<Student, Integer> anotherMap = new HashMapImpl<>();
        map.put(OLEG, 20);
        anotherMap.put(NICK, 20);

        assertThat(map).isNotEqualTo(anotherMap);
    }

    @Test
    void equalsAndHashcode_shouldBeSymmetric() {
        Map<Student, Integer> anotherMap = new HashMapImpl<>();
        map.put(OLEG, 20);
        anotherMap.put(OLEG, 20);

        assertThat(map.equals(anotherMap) && anotherMap.equals(map)).isTrue();
        assertThat(map).hasSameHashCodeAs(map);

    }

    @Test
    void whenPutTwoDifferentEntries_SizeShouldBeTwo() {
        map.put(NICK, 19);
        map.put(OLEG, 19);

        assertThat(map).hasSize(2);
    }

    @Test
    void whenPutTwoSameEntries_SizeShouldBeOne() {
        map.put(NICK, 19);
        map.put(NICK, 20);

        assertThat(map).hasSize(1);
    }

    @Test
    void tableShouldBeResized() {
        int numberOfEntries = 80;
        for (int i = 1; i <= numberOfEntries; i++) {
            map.put(new Student("Nick" + i, LocalDate.of(2001, 8, 28), "intern"), 19);
        }

        assertThat(map).hasSize(numberOfEntries);
        for (int i = 1; i <= numberOfEntries; i++)
            assertThat(map).containsKey(new Student("Nick" + i, LocalDate.of(2001, 8, 28), "intern"));
    }

    @Test
    void whenPutEntryAndGetIt_shouldReturnValue() {
        map.put(NICK, 19);

        assertThat(map).containsEntry(NICK, 19);
    }

    @Test
    void whenPutTwoEntriesAndGetOne_shouldReturnCorrectValue() {
        map.put(NICK, 19);
        map.put(OLEG, 20);

        assertThat(map).containsEntry(OLEG, 20);
    }

    @Test
    void whenPutEntry_shouldReturnPreviousValue() {

        Student nick6 = new Student("Nick6", LocalDate.of(2001, 8, 28), "intern");
        map.put(nick6, 19);
        Student nick11 = new Student("Nick11", LocalDate.of(2001, 8, 28), "intern");
        map.put(nick11, 20);
        Integer val = map.put(nick11, 99);

        assertThat(map).hasSize(2);
        assertThat(val).isEqualTo(20);
        assertThat(map).containsEntry(nick11, 99);
    }

    @ParameterizedTest
    @MethodSource("getNonExistentKeys")
    void whenPutEntryAndGetNonExistentKey_shouldReturnNull(Object key) {
        map.put(NICK, 19);

        assertThat(map.get(key)).isNull();
    }

    @Test
    void whenGetNonExistentKey_shouldReturnNull() {
        assertThat(map.get(OLEG)).isNull();
    }

    public static Stream<Object> getNonExistentKeys() {
        return Stream.of(
                1,
                OLEG,
                null
        );
    }

    @Test
    void shouldContainKey() {
        map.put(OLEG, 10);

        assertThat(map).containsKey(OLEG);
    }

    @Test
    void shouldNotContainKey() {
        assertThat(map.containsKey(NICK)).isFalse();
    }

    @Test
    void shouldReturnValues() {
        map.put(NICK, 19);
        map.put(OLEG, 20);

        assertThat(map.values()).containsAll(List.of(19, 20));
    }

    @Test
    void shouldNotReturnValues() {
        assertThat(map.values()).isEmpty();
    }

    @Test
    void shouldReturnKeySet() {
        map.put(NICK, 19);
        map.put(OLEG, 20);

        assertThat(map.keySet()).containsAll(Set.of(NICK, OLEG));
    }

    @Test
    void shouldNotReturnKeySet() {
        assertThat(map.keySet().size()).isZero();
    }

    @Test
    void shouldContainValue() {
        map.put(NICK, 19);

        assertThat(map).containsValue(19);
    }

    @Test
    void shouldNotContainValue() {
        map.put(NICK, 20);

        assertThat(map.containsValue(19)).isFalse();
    }

    @Test
    void afterPutAndRemoveEntry_mapShouldBeEmpty() {
        map.put(NICK, 20);
        map.remove(NICK);

        assertThat(map).isEmpty();
    }

    @Test
    void afterMultipleRemoveEntry_mapShouldBeEmpty() {
        for (int i = 0; i < 10; i++) {
            map.put(new Student("Nick" + i, LocalDate.of(2001, 8, 28), "intern"), 10);
        }
        map.put(NICK, null);
        map.remove(NICK);
        for (int i = 0; i < 10; i++) {
            map.remove(new Student("Nick" + i, LocalDate.of(2001, 8, 28), "intern"));
        }

        assertThat(map).isEmpty();
    }

    @Test
    void afterRemoveEntry_shouldReturnNull() {
        map.put(NICK, null);
        Integer val = map.remove(NICK);

        assertThat(val).isNull();
    }

    @Test
    void afterRemoveEntry_shouldNotReturnNull() {
        map.put(NICK, 21);
        Integer val = map.remove(NICK);

        assertThat(val).isEqualTo(21);
        assertThat(map).isEmpty();
    }

    @Test
    void whenRemoveWithNoMapping_shouldReturnNull() {
        map.put(NICK, 21);
        Integer val = map.remove(OLEG);

        assertThat(val).isNull();
        assertThat(map).hasSize(1);
    }

    @Test
    void whenRemove_shouldNotBeEmpty() {
        map.put(NICK, 21);
        map.put(OLEG, 20);
        Integer val = map.remove(OLEG);

        assertThat(map)
                .hasSize(1)
                .isNotEmpty();

    }

    @Test
    void shouldPutAll() {
        map.put(NICK, 21);
        map.put(OLEG, 20);
        Map<Student, Integer> hashMap = new HashMap<>();
        hashMap.put(NICK, 99);
        hashMap.put(JOHN, 23);
        map.putAll(hashMap);

        assertThat(map)
                .hasSize(3)
                .containsEntry(NICK, 99);
    }

    @Test
    void shouldPutAllNothing() {
        map.put(NICK, 21);
        map.put(OLEG, 20);
        Map<Student, Integer> hashMap = new HashMap<>();
        map.putAll(hashMap);

        assertThat(map).hasSize(2);
    }

    @Test
    void whenPutAll_ShouldThrowException() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            map.putAll(null);
        });
    }

    @Test
    void shouldBeAbleToClear() {
        map.put(NICK, 21);
        map.put(OLEG, 20);
        map.clear();

        assertThat(map).isEmpty();

    }

    @Test
    void constructorShouldThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            new HashMapImpl<Student, Integer>(null);
        });
    }

    @Test
    void constructorShouldThrowIllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new HashMapImpl<Student, Integer>(-1);
        });
    }

    @Test
    void shouldBeAbleToPutNullTwoTimes() {
        map.put(null, 10);
        map.put(null, 20);

        assertThat(map).containsEntry(null, 20);
    }

    @Test
    void whenPutInMapWithZeroCapacity_shouldBeNotEmpty() {
        Map<Integer, Integer> map = new HashMapImpl<>(0);
        map.put(1, 1);

        assertThat(map).isNotEmpty();
    }
}