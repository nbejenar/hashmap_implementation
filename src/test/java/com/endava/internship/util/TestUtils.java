package com.endava.internship.util;

import com.endava.internship.Student;

import java.time.LocalDate;


public class TestUtils {
    public static Student OLEG = new Student("Oleg", LocalDate.of(2001, 8, 28), "intern");
    public static Student NICK = new Student("Nick", LocalDate.of(2001, 8, 28), "intern");
    public static Student JOHN = new Student("JOHN", LocalDate.of(2001, 8, 28), "intern");
}